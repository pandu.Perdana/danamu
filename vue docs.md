API login / get token API

	POST http://localhost:8080/oauth/token?grant_type=password&username=<user login name>&password=<user login password>
	Authorization Basic
	username myclientId
	password mysecret
	output :
	{
    "access_token": "1fb180ac-d7af-47b9-9fb1-1e529e2d5ef3",
    "token_type": "bearer",
    "refresh_token": "fab41dc2-ab82-493c-95dd-3d42ba79c6f8",
    "expires_in": 21599,
    "scope": "read write"
	}
	
API list campaign for user

	GET http://localhost:8080/api/publishedcampaign

API list campaign for admin

	GET http://localhost:8080/api/allcampaign
	Authorization Bearer <token>

API get persentase progress donasi berdasar campaign id

	GET http://localhost:8080/api/donationprogress/<campaign id>
	Authorization Bearer <token>

API update campaign

	POST http://localhost:8080/api/updcampaign
	Authorization Bearer <token>
	body : 
	{
		"id":<id campaign>,
		"title":<title> ,
		"description":<description>,
		"amountDonation":<numeric>,
		"amountTarget":<numeric>,
		"startDate":"<YYYY-MM-DD>",
		"endDate":"<YYYY-MM-DD>",
		"beneficiary":<penerima manfaat>
	}

API create campaign

	POST http://localhost:8080/api/addcampaign
	Authorization Bearer <token>
	body : 
	{
		"title":<title> ,
		"description":<description>,
		"amountDonation":<numeric>,
		"amountTarget":<numeric>,
		"startDate":"<YYYY-MM-DD>",
		"endDate":"<YYYY-MM-DD>",
		"beneficiary":<penerima manfaat>
	}
	keterangan: tanpa id. sbb id auto increment

API list pending transaction	

	GET http://localhost:8080/api/pendingtrans
	Authorization Bearer <token>
	output:
	[
    {
        "id": 2,
        "campaignId": 2,
        "donorName": "donor name 24",
        "donorPhoneNo": "1234567890",
        "amount": 2220,
        "paymentTypeId": 1,
        "status": "PENDING",
        "bankId": 1
    },
    {
        "id": 6,
        "campaignId": 2,
        "donorName": "donor name 4",
        "donorPhoneNo": "1234567890",
        "amount": 2880,
        "paymentTypeId": 1,
        "status": "PENDING",
        "bankId": 1
    }
	]
	
API list Bank

	GET http://localhost:8080/api/banks
	output:
	[
    {
        "id": 1,
        "name": "BCA",
        "description": "Bank Central Asia"
    },
    {
        "id": 2,
        "name": "BNI",
        "description": "BNI Desc"
    }
	]
	
API Update transaction status into verified

	GET http://localhost:8080/api/transverified/{transaction id}
	Authorization Bearer <token>
	output
	{
    "id": 1,
    "campaignId": 2,
    "donorName": "donor name 1",
    "donorPhoneNo": "1233545465",
    "amount": 2220,
    "paymentTypeId": 1,
    "status": "VERIFIED",
    "bankId": 2
	}

API for display image

	GET http://localhost:8080/getimg/{file name}
	
API increment campaign share counter

	GET http://localhost:8080/api/campaignshareinc/{campaign Id}
	Authorization Bearer <token>
	output
	Update Success.
	
API change campaign status into approved

	GET http://localhost:8080/api/campaignapproved/{campaign Id}
	Authorization Bearer <token>
	output
	Update Success.
	
API add transaction verification

	POST http://localhost:8080/api/addTransVerify
	Authorization Bearer <token>
	body : 
	{
		"id":"5",
		"bankId":"1",
		"amount":"2000",
		"accountOwner":"account owner 2",
		"transDate":"2020-01-20"
	}

API update transaction verification

	POST http://localhost:8080/api/updTransVerify
	Authorization Bearer <token>
	body : 
	{
		"id":"5",
		"bankId":"1",
		"amount":"2000",
		"accountOwner":"account owner 2",
		"transDate":"2020-01-20"
	}
	
API search campaign by keyword
	
	GET http://localhost:8080/api/findcampaign/{keyword}
	output
	[
    {
        "id": 1,
        "title": "title 2",
        "description": "description 2",
        "imagePath": null,
        "amountDonation": 2000,
        "amountTarget": 25000,
        "startDate": 1579453200000,
        "endDate": 1608397200000,
        "fundraiser": null,
        "beneficiary": "beneficiary 2",
        "status": "APPROVED",
        "shareCounter": null
    },
    {
        "id": 5,
        "title": "title 15",
        "description": "description 15",
        "imagePath": null,
        "amountDonation": 2000,
        "amountTarget": 25000,
        "startDate": 1579453200000,
        "endDate": 1608397200000,
        "fundraiser": null,
        "beneficiary": "beneficiary 15",
        "status": "APPROVED",
        "shareCounter": null
    }
	]

API add user
	
	POST http://localhost:8080/user
	Authorization Bearer <token>
	body : 
	{
		"userName":"user3",
		"password":"user3",
		"role":"ADMIN",
		"enabled":"1"
	}

	