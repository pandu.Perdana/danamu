import Axios from "axios";
import router from "../router"

const environmentURL = process.env.VUE_APP_BASEURI;
const environmentNode = process.env.NODE_ENV;
console.log("vue_app_baseuri", environmentURL);
console.log("node_env", environmentNode);

const url = {
    baseURL: environmentURL
};

const HTTP = Axios.create({
    baseURL: url.baseURL,
    timeout: 10000
})

export default HTTP;